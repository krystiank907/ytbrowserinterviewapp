//
//  YTMainListViewController.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import UIKit
import SnapKit

class YTMainListViewController: YTViewController<VMMainListScheme> {
    
    private let tableView: UITableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        makeConstraints()
    }
    
    private func fetchData() {
        viewModel.getData { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.errorHandler?.throw(error)
            case .success:
                self?.tableView.reloadData()
            }
        }
    }
    
    private func setupViews() {
        viewModel.activityIndicatorHandler = { [weak self] value in
            self?.handleByDefaultActivityIndicator(value)
        }
        viewModel.shouldReloadData = { [weak self] in
            self?.fetchData()
        }
        viewModel.setupActivityService()
        viewModel.checkForUpdate()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.register(YTMainListTableViewCell.self, forCellReuseIdentifier: YTMainListTableViewCell.standardIdentifier)
    }
    
    private func makeConstraints() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
}

extension YTMainListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: YTMainListTableViewCell.standardIdentifier) as? YTMainListTableViewCell, let cellData = viewModel.cellData(at: indexPath) else {
            return UITableViewCell()
        }
        cell.setupCellData(data: cellData)
        return cell
    }
    
    
}
