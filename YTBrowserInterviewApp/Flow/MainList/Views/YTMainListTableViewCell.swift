//
//  YTMainListTableViewCell.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import UIKit
import AlamofireImage

struct YTMainListTableViewCellData {
    var title: String
    var description: String
    var urlString: String
}

class YTMainListTableViewCell: UITableViewCell {
    
    var iconImageView: UIImageView = UIImageView()
    var titleLabel: UILabel = UILabel()
    var descriptionLabel: UILabel = UILabel()
    var activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        selectionStyle = .none
        titleLabel.font = .systemFont(ofSize: 19, weight: .bold)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .orange
        descriptionLabel.font = .systemFont(ofSize: 16, weight: .light)
        descriptionLabel.textColor = .lightGray
        descriptionLabel.numberOfLines = 0
        activityIndicatorView.tintColor = .orange
    }
    
    private func makeConstraints() {
        contentView.addSubview(iconImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        iconImageView.addSubview(activityIndicatorView)
        iconImageView.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.height.equalTo(80)
            make.leading.equalToSuperview().offset(20)
            make.top.greaterThanOrEqualToSuperview().offset(20)
            make.centerY.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.leading.equalTo(iconImageView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-20)
        }
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(titleLabel.snp.trailing)
            make.bottom.lessThanOrEqualToSuperview()
        }
        activityIndicatorView.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
    }
    
    func setupCellData(data: YTMainListTableViewCellData) {
        titleLabel.text = data.title
        descriptionLabel.text = data.description
        if let url = URL(string: data.urlString) {
            activityIndicatorView.startAnimating()
            iconImageView.af.setImage(withURL: url, completion: { [weak self] (response) in
                self?.activityIndicatorView.stopAnimating()
                self?.iconImageView.image = response.value
            })
        }
    }
    
    
}
