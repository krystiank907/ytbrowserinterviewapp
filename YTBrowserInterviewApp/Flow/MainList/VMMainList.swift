//
//  VMMainList.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation
import RealmSwift

protocol VMMainListScheme: YTNetworkViewModelScheme {
    var numberOfRows: Int { get }
    var shouldReloadData: YTEmptyHandler { get set }
    func getData(completion: @escaping ResultCompletion<Void>)
    func cellData(at indexPath: IndexPath) -> YTMainListTableViewCellData?
    func checkForUpdate()
}

class VMMainList: VMMainListScheme {
    
    var apiData: YTChannelsListDTO?
    var activityService: NetworkActivityServiceScheme?
    var activityIndicatorHandler: YTValueHandler<Bool> = nil
    var shouldReloadData: YTEmptyHandler = nil
    
    init() {
        let realm = try? Realm()
        apiData = realm?.objects(YTChannelsListDTO.self).first
    }
    
    var numberOfRows: Int {
        return apiData?.items.count ?? 0
    }
    
    func checkForUpdate() {
        guard let apiData = apiData else {
            shouldReloadData?()
            return
        }
        let components = minutesBetweenDates(apiData.lastUpdateDate, Date())
        if components > 5 {
            shouldReloadData?()
        }
    }
    
    private func minutesBetweenDates(_ oldDate: Date, _ newDate: Date) -> CGFloat {
        let newDateMinutes = newDate.timeIntervalSinceReferenceDate / 60
        let oldDateMinutes = oldDate.timeIntervalSinceReferenceDate / 60
        return CGFloat(newDateMinutes - oldDateMinutes)
    }
    
    func getData(completion: @escaping ResultCompletion<Void>) {
        apiClient.request(router: YTChannelsRouter.getChannels, activityService: activityService) { [weak self] (result: Result<YTChannelsListResponse>) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let response):
                guard let data = response.data else {
                    completion(.failure(APIClientError.commonError))
                    return
                }
                self?.apiData = data
                completion(.success(()))
            }
        }
    }
    
    func cellData(at indexPath: IndexPath) -> YTMainListTableViewCellData? {
        guard let channelItem = apiData?.items.sorted(by: { $0.snippet?.snippetDescription.count ?? 0 < $1.snippet?.snippetDescription.count ?? 0 })[safe: indexPath.row] else { return nil }
        let title = channelItem.snippet?.title ?? ""
        let desc = channelItem.snippet?.snippetDescription ?? ""
        let utrString = channelItem.snippet?.thumbnails?.thumbnailsDefault?.url ?? ""
        return YTMainListTableViewCellData(title: title, description: desc, urlString: utrString)
    }
}
