//
//  YTWorld.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation
import Alamofire

protocol YTWorldScheme {
    static var shared: YTWorldScheme { get }

    var apiClient: YTClientService { get }
    var apiClientConfiguration: YTClientConfiguration { get }

}

struct YTWorld {
    let apiClient: YTClientService
    let apiClientConfiguration: YTClientConfiguration

    private init(apiClient: YTClientService, apiClientConfiguration: YTClientConfiguration) {
        self.apiClient = apiClient
        self.apiClientConfiguration = apiClientConfiguration
        self.apiClient.setConfiguration(apiClientConfiguration)
    }
}

extension YTWorld: YTWorldScheme {
    
    static var shared: YTWorldScheme = {
        let baseUrl = "http://api.norbsoft.com/sciTube/v2"
        return YTWorld(apiClient: APIClientService.shared,
                       apiClientConfiguration: APIClientConfiguration(baseUrl: baseUrl))
    }()
    
}
