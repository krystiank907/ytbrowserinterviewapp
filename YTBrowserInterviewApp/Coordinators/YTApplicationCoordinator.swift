//
//  YTApplicationCoordinator.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import UIKit

class YTApplicationCoordinator: YTBaseCoordinator<Void> {
    
    private let container = YTDependencyInjection.createChild()
    
    override func start() {
        showMain()
    }
    
    private func showMain() {
        guard let mainController = container.resolve(YTMainListViewController.self) else { return }
        mainController.errorHandler = defaultErrorHandler
        mainController.navigationItemConfiguration = CommonNavigationBarConfiguration(title: "loc_main_title".localized)
        navigationController.viewControllers = [mainController]
    }
    
}
