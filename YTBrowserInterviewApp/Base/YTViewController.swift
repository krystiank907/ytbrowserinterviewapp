//
//  YTViewController.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import UIKit
import ProgressHUD

protocol YTBaseViewModel { }

class YTViewController<TViewModel>: UIViewController {
    
    var navigationItemConfiguration: NavigationBarConfigurationScheme?
    var errorHandler: YTErrorHandleable?
    var viewModel: TViewModel!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItemConfiguration?.configureNavigationItem(self.navigationItem)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(navigationItemConfiguration == nil, animated: animated)
    }
    
    func handleByDefaultActivityIndicator(_ value: Bool, with title: String? = "loc_loading".localized) {
        value ? ProgressHUD.show(title) : ProgressHUD.dismiss()
    }
    
    
}
