//
//  YTNetworkViewModelScheme.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation

protocol YTNetworkViewModelScheme: class, YTBaseViewModel {
    var apiClient: YTClientService { get }
    var activityService: NetworkActivityServiceScheme? { get set }
    var activityIndicatorHandler: YTValueHandler<Bool> { get set }
    func setupActivityService()
}

extension YTNetworkViewModelScheme {
    var apiClient: YTClientService {
        return YTWorld.shared.apiClient
    }
}

extension YTNetworkViewModelScheme {
    func setupActivityService() {
        self.activityService = NetworkActivityService(onActivityStart: { [weak self] in
            self?.activityIndicatorHandler?(true)
        }, onActivityEnd: { [weak self] in
            self?.activityIndicatorHandler?(false)
        })
    }
}

