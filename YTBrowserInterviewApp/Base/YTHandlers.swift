//
//  YTHandlers.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation

typealias YTEmptyHandler = (() -> Void)?
typealias YTValueHandler<T> = ((T) -> Void)?
typealias YTOptionalValueHandler<T> = ((T?) -> Void)?
