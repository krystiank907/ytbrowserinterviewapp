//
//  YTDependencyInjection.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation
import Swinject

class YTDependencyInjection {
    
    let container = Container()
    static let shared: YTDependencyInjection = YTDependencyInjection()
    
    class func createChild() -> Container {
        return Container(parent: shared.container)
    }
    
    init() {
        setupMainFlow()
    }
    
    private func setupMainFlow() {
        container.register(VMMainListScheme.self) { _ in VMMainList() }
        container.register(YTMainListViewController.self) { res -> YTMainListViewController in
            let viewController = YTMainListViewController()
            viewController.viewModel = res.resolve(VMMainListScheme.self)
            return viewController
        }
    }
    
}


