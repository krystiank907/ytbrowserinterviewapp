//
//  YTChannelsRouter.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation
import Alamofire

enum YTChannelsRouter: Router {
    case getChannels
    
    var path: String {
        switch self {
        case .getChannels:
            return "/channels.json"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getChannels:
            return .get
        }
    }

    var parameters: Parameters {
        return [:]
    }

    var headers: HTTPHeaders? {
        return nil
    }

}
