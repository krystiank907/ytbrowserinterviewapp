//
//  YTChannelsListResponse.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation

struct YTChannelsListResponse: Response {
    typealias CodableScheme = YTChannelsListDTO

    var data: YTChannelsListDTO?
    var `internal`: HTTPURLResponse?
    var url: URL?

    init() {
        
    }
    
}
