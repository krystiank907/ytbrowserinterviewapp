//
//  YTChannelsListDTO.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation
import RealmSwift

class YTChannelsListDTO: Object, Codable {
    @objc dynamic var id: String = "1"
    @objc dynamic var lastUpdateDate: Date = Date()
    var items = List<YTChannelsItemDTO>()
    
    enum CodingKeys: String, CodingKey {
        case items
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let itemsList = try container.decode([YTChannelsItemDTO].self, forKey: .items)
        items.append(objectsIn: itemsList)
    }
}

class YTChannelsItemDTO: Object, Codable {
    //swiftlint:disable:next redundant_optional_initialization
    @objc dynamic var snippet: YTChannelsItemSnippetDTO? = nil
    
    enum CodingKeys: String, CodingKey {
        case snippet
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        snippet = try container.decode(YTChannelsItemSnippetDTO.self, forKey: .snippet)
    }
}

class YTChannelsItemSnippetDTO: Object, Codable {
    @objc dynamic var publishedAt: String = ""
    @objc dynamic var channelID: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var snippetDescription: String = ""
    //swiftlint:disable:next redundant_optional_initialization
    @objc dynamic var thumbnails: YTChannelsItemThumbnailsDTO? = nil
    @objc dynamic var channelTitle: String = ""

    enum CodingKeys: String, CodingKey {
        case publishedAt
        case channelID = "channelId"
        case title
        case snippetDescription = "description"
        case thumbnails, channelTitle
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        publishedAt = try container.decode(String.self, forKey: .publishedAt)
        channelID = try container.decode(String.self, forKey: .channelID)
        title = try container.decode(String.self, forKey: .title)
        snippetDescription = try container.decode(String.self, forKey: .snippetDescription)
        channelTitle = try container.decode(String.self, forKey: .channelTitle)
        thumbnails = try container.decode(YTChannelsItemThumbnailsDTO.self, forKey: .thumbnails)
    }

}

class YTChannelsItemThumbnailsDTO: Object, Codable {
    //swiftlint:disable:next redundant_optional_initialization
    @objc dynamic var thumbnailsDefault: YTChannelsItemDefaultDTO? = nil

    enum CodingKeys: String, CodingKey {
        case thumbnailsDefault = "default"
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        thumbnailsDefault = try container.decode(YTChannelsItemDefaultDTO.self, forKey: .thumbnailsDefault)
    }
}


class YTChannelsItemDefaultDTO: Object, Codable {
    @objc dynamic var url: String = ""
    
    enum CodingKeys: String, CodingKey {
        case url
    }
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        url = try container.decode(String.self, forKey: .url)
    }

}
