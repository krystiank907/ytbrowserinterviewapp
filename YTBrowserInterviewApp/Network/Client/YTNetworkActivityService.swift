//
//  YTNetworkActivityService.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation

protocol NetworkActivityServiceScheme {
    init(dispatchGroup: DispatchGroup?, onActivityStart: YTEmptyHandler, onActivityEnd: YTEmptyHandler, onAllActivitesEnd: YTEmptyHandler)

    func start()
    func stop()
    func notify()
}

class NetworkActivityService: NetworkActivityServiceScheme {
    private let onActivityStart: YTEmptyHandler
    private let onActivityEnd: YTEmptyHandler
    private let onAllActivitesEnd: YTEmptyHandler
    private let dispatchGroup: DispatchGroup?

    required init(dispatchGroup: DispatchGroup? = DispatchGroup(),
                  onActivityStart: YTEmptyHandler,
                  onActivityEnd: YTEmptyHandler,
                  onAllActivitesEnd: YTEmptyHandler = nil) {
        self.dispatchGroup = dispatchGroup
        self.onActivityStart = onActivityStart
        self.onActivityEnd = onActivityEnd
        self.onAllActivitesEnd = onAllActivitesEnd
    }

    func start() {
        dispatchGroup?.enter()
        onActivityStart?()
    }

    func stop() {
        dispatchGroup?.leave()
        onActivityEnd?()
    }

    func notify() {
        dispatchGroup?.notify(queue: .main, execute: { [weak self] in
            self?.onAllActivitesEnd?()
        })
    }
}
