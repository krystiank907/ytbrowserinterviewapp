//
//  YTErrors.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import Foundation

enum APIClientError: Error, Equatable {
    case networkError
    case commonError
}

