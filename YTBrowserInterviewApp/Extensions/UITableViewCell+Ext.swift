//
//  UITableViewCell+Ext.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import UIKit

extension UITableViewCell {
    static var standardIdentifier: String {
        return String(describing: self)
    }
}

