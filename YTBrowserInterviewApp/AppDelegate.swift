//
//  AppDelegate.swift
//  YTBrowserInterviewApp
//
//  Created by Krystian Kulawiak on 21/01/2021.
//

import UIKit
import RealmSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    var applicationCoordinator: YTApplicationCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
        NetworkActivityLogger.shared.startLogging()
        #endif
        setupAppearance()
        createRootViewController()
        return true
    }

    private func createRootViewController() {
        if window == nil {
            window = UIWindow()
        }
        window?.makeKeyAndVisible()
        let rootVC = YTBaseNavigationController()
        window?.rootViewController = rootVC
        applicationCoordinator = YTApplicationCoordinator(navigationController: rootVC)
        applicationCoordinator?.start()
    }

    private func setupAppearance() {
        let navBarAppearance = UINavigationBar.appearance()
        navBarAppearance.barTintColor = .orange
        navBarAppearance.tintColor = .white
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
    }

}

